# Home Assistant

[![Circle CI](https://circleci.com/gh/nlamirault/home-assistant-configuration/tree/master.svg?style=svg)](https://circleci.com/gh/nlamirault/home-assistant-configuration/tree/master)

This is my Home Assistant configuration.
I'm currently running Home Assistant version __0.51.2__. Installation used is [docker-compose](https://docs.docker.com/compose/), running on a Raspberry Pi 3, with [HypriotOS](https://blog.hypriot.com/downloads/)

When the HA configuration is ready, Kubernetes will be the deployment platform (See [Jarvis](https://github.com/zeiot/jarvis))

| Use                    | Device                         |
|------------------------|--------------------------------|
| Box TV                 | LiveboxTV by Orange            |
| Home Assistant         | Raspberry PI 3 (Docker image)  |
| NAS                    | Synology DS212j                |
| Media Players          | Kodi (OSMC) x2                 |
| Phones and tablets     | Android, iOS                   |
| Computers              | iMAC, Linux, ...               |
| Domotic                | Xiaomi gateway                 |
| Door sensors           | Xiaomi door sensors x3         |
| Motion sensors         | Xiaomi motion sensors x1       |
| Temperature sensors    | Xiaomi Temperature sensors x4  |


## Automations

### Security

* [x] Notify when phone are connected/disconnected to the wifi
* [ ] Notify when a new device is connected to the wifi
* [ ] Notify when the doors are opened
* [ ] Notify when there is motion

### System Monitoring

* [x] Notify when Home Assistant has restarted
* [x] Notify when public IP address has changed

## Screenshots



## Contact

Nicolas Lamirault <nicolas.lamirault@gmail.com>
